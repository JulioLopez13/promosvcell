import axios from "axios";
let server = 'http://165.227.199.69:8092/api/';
//const server = 'http://localhost:8092/api/';
//let server = 'http://192.168.50.54:8080/api/';
//let server = 'http://192.168.50.244:8090/api/'

const server2 = server
/**Methods**/
export const POST = 'post';
export const GET = 'get';
export const PATCH = 'patch';
export const DELETE = 'delete';
export const PUT = 'put';
const ROUTE = 'contactMail'

const RECEIVER = 'printhouse@vcell.com.mx'
const APIKEY = 'FcH<4d}ce3T,);EY'
const SENDER = 'contacto@fyf.mx'
const SUBJECT = 'Petición de información'
/**Routes**/
const VERSION = "version";

/**Config Builder
 * @constructor
 * @param requestMethod{string}is the request method to be used when making the request -  Ej 'post'
 * @param route{string}  is the server URL that will be used for the request - Ej '/quienSoy'
 * @param params{string} are the URL parameters to be sent with the request -  Ej ['id','day']
 * @param data{Object} is the data to be sent as the request body -Only applicable for request methods 'PUT', 'POST', and 'PATCH'
 * @param headers
 * @param isFormData
 * **/
function config(requestMethod, route, data, params, headers, isFormData) {
  let auxHeaders = {authorization: `Bearer ${window.localStorage.token}`}
  if (isFormData) auxHeaders = {
    authorization: `Bearer ${window.localStorage.token}`,
    'content-type': 'multipart/form-data'
  }
  return {
    url: route,
    method: requestMethod, // default
    // `baseURL` serverURL
    // Ej ''http://serverBonito:8080
    baseURL: server,
    // `headers` are custom headers to be sent
    // Ej  'authorization': `Bearer ${window.localStorage.token}`
    headers: auxHeaders,
    params: params,
    // `data` is the data to be sent as the request body
    // Only applicable for request methods 'PUT', 'POST', and 'PATCH'
    // When no `transformRequest` is set, must be of one of the following types:
    // - string, plain object, ArrayBuffer, ArrayBufferView, URLSearchParams
    // - Browser only: FormData, File, Blob
    // - Node only: Stream, Buffer
    // Ej {msj:"holis"}
    data: data,
  }
}

export function execute(method, route, data, params, headers, responseType, server, isFormData) {
  return new Promise((resolve, reject) => {
    let conf = new config(method, route, data, params, headers, isFormData);
    if (server) conf.baseURL = server;
    if (responseType) conf.responseType = responseType;
    if (headers) Object.keysProp(headers).forEach(k => conf.headers[k] = headers[k]);
    awesomeLog("Request", {
      url: server2+route
    });
    const aux = axios.request(conf)
      .then(r => {
        awesomeLog("Response", r);
        resolve(r.data)
      })
      .catch(e => {
        console.log(aux)

        awesomeLog("Error", e.response);
        reject({message: e.response.data.message, status: e.response.status})
      });

  })
}

export function sendEmail({data,email}) {
  data.apiKey = APIKEY
  data.receiver = RECEIVER
  data.subject = SUBJECT
  data.sender = SENDER
  data.message += `\n${email}`
  console.log(data)
  return execute(POST, ROUTE, data)
}

function awesomeLog(titulo, obj) {
  console.log("*******" + titulo + "************");
  console.log(obj);
  console.log("*******Fin " + titulo + "*********");
  //if(obj.data && obj.data.message) M.toast({html: obj.data.message})
}


